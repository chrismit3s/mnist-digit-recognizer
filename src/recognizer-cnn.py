import keras
from keras import models, layers, datasets

# output magic
print_normal = print
print_padded = lambda *args, **kwargs: (print_normal("  ", end=""), print_normal(*args, **kwargs))
print_header = lambda *args, **kwargs: (print_normal(), print_normal(*args, **kwargs))
__builtins__.print = print_padded

# constants
batch_size = 100
epochs = 20
num_classes = 10
optimizer = "adadelta"
loss = "categorical_crossentropy"
metrics = ["accuracy"]

# load and prepare data
print_header("DATASET:")
data_format = keras.backend.image_data_format()
input_shape = (1, 28, 28) if data_format == "channels_first" else (28, 28, 1)
# load dataset
print_padded("loading data", end="")
(x_train, y_train), (x_test, y_test) = datasets.mnist.load_data()
print_normal(" - done")
# reshape, retype and downscale x values
print_padded("preparing x values", end="")
x_train = x_train.reshape(x_train.shape[0], *input_shape).astype("float32") / 255
x_test = x_test.reshape(x_test.shape[0], *input_shape).astype("float32") / 255
print_normal(" - done")
# transform y values to one-hot vectors
print_padded("preparing y values", end="")
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)
print_normal(" - done")
print(f"training samples - {x_train.shape[0]}")
print(f"testing samples  - {x_test.shape[0]}")


# variables
print_header("VARIABLES:")
print_padded(f"num_classes = {num_classes}")
print_padded(f"input_shape = {input_shape}")
print_padded(f"batch_size  = {batch_size}")
print_padded(f"epochs      = {epochs}")
print_padded(f"data_format = {data_format}")
print_padded(f"optimizer   = {optimizer}")
print_padded(f"loss        = {loss}")
print_padded(f"metrics     = {', '.join(metrics)}") # comma seperated list of metric values

# create the model
print_header("MODEL CREATION:")
model = models.Sequential()
# convolutional layers
print_padded("convolutional layers", end="")
model.add(layers.Conv2D(filters= 32, kernel_size=(5, 5), activation="relu", data_format=data_format, input_shape=input_shape))
model.add(layers.Conv2D(filters= 64, kernel_size=(5, 5), activation="relu"))
model.add(layers.Conv2D(filters=128, kernel_size=(5, 5), activation="relu"))
print_normal(" - done")
# max pooling
print_padded("max pooling layer", end="")
model.add(layers.MaxPool2D(pool_size=(2, 2), data_format=data_format))
print_normal(" - done")
# flatten
print_padded("flatten layer", end="")
model.add(layers.Flatten())
print_normal(" - done")
# dense layers
print_padded("dense layers", end="")
model.add(layers.Dense(units=256, activation="relu"))
model.add(layers.Dense(units=128, activation="relu"))
model.add(layers.Dense(units= 64, activation="relu"))
model.add(layers.Dense(units= 32, activation="relu"))
model.add(layers.Dense(units= 16, activation="relu"))
model.add(layers.Dense(units=num_classes, activation="softmax"))
print_normal(" - done")

# model summary
print_header("MODEL SUMMARY:")
model.summary(print_fn=lambda *args, **kwargs: print(*args, **kwargs) if args[0].replace("_", "") != "" else None)

# compile model
model.compile(optimizer=optimizer,
              loss=loss,
              metrics=metrics)

# train model
print_header("MODEL TRAINING:")
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))

